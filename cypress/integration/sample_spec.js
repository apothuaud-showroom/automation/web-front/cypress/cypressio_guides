/// <reference types="Cypress" />

describe('My first test', function() {
    it('Does not do much', function() {
        expect(true).to.equal(true);
    }),
    it('Visits the kitchen sink', function() {
        cy.visit('https://example.cypress.io/');
    }),
    it('Finds an element', function() {
        cy.visit('https://example.cypress.io/');
        cy.contains('type');
    }),
    it('Clicks an element', function() {
        cy.visit('https://example.cypress.io/');
        cy.contains('type').click();
    }),
    it('Makes an assertion', function() {
        cy.visit('https://example.cypress.io/');
        cy.contains('type').click();
        cy.url().should('include', '/commands/actions');
    }),
    it('Gets, types and asserts', function() {
        cy.visit('https://example.cypress.io/');
        cy.contains('type').click();
        cy.url().should('include', '/commands/actions');
        cy.get('.action-email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com');
    })
})